package com.galoppo.brainstorm;

import com.galoppo.brainstorm.protocol.Message;
import com.galoppo.brainstorm.protocol.server.ServerMessageListener;
import com.galoppo.brainstorm.protocol.server.ServerMessageProcessor;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import com.galoppo.brainstorm.net.NetUtil;
import com.galoppo.brainstorm.protocol.ProtoListener;
import com.galoppo.brainstorm.protocol.SharedObject;
import com.galoppo.brainstorm.widget.WidgetObject;
import java.awt.Point;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Server implements ServerMessageListener {
    private final List<Socket> clients = new LinkedList<>();
    private final Map<String, SharedObject> sharedObjects = new HashMap<>();
    private final Map<String, Socket> locks = new HashMap<>();
    
    public void addClient(Socket client) {
        synchronized(clients) {
            clients.add(client);
        }
        new Thread(new ProtoListener(client, new ServerMessageProcessor(this))).start();
    }
    
    @Override
    public void onTextMessage(Socket sourceSocket, Message message) {
        broadcast(message);
    }
    
    @Override
    public void onRefreshRequest(Socket sourceSocket, Message message) {
        synchronized (sharedObjects) {
            for (SharedObject obj : sharedObjects.values()) {
                try {
                    NetUtil.sendMessage(sourceSocket, Message.TYPE_SHARED_UPD, obj);
                } catch(Exception ex) {
                    
                }
            }
        }        
    }
    
    @Override
    public void onLockRequest(Socket sourceSocket, Message message) {
        boolean grantLock = false;
        String objId = new String(message.getBytes());
        synchronized (locks) {
            Socket owner = locks.get(objId);
            if (null == owner || owner.equals(sourceSocket)) {
                locks.put(objId, sourceSocket);
                grantLock = true;
            }
        }
        int type = grantLock ? Message.TYPE_SHARED_LOCK : Message.TYPE_LOCK_DENY;
        try {
            NetUtil.sendMessage(sourceSocket, type, objId.getBytes());
        } catch(Exception ex) {
            
        }
    }
    
    @Override
    public void onLockRelease(Socket sourceSocket, Message message) {
        String objId = new String(message.getBytes());
        synchronized (locks) {
            Socket owner = locks.get(objId);
            if (null != owner && owner.equals(sourceSocket)) {
                locks.remove(objId);
                try {
                    NetUtil.sendMessage(sourceSocket, Message.TYPE_SHARED_UNLOCK, objId.getBytes());
                } catch(Exception ex){
                    
                }
            }
        }        
    }
    
    @Override
    public void onSharedObjectCreate(Socket sourceSocket, Message message) {
        try {
            SharedObject object = SharedObject.fromByteArray(message.getBytes());
            synchronized (sharedObjects) {
                sharedObjects.put(object.getId(), object);
            }
            broadcast(message);        
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public void onSharedObjectUpdate(Socket sourceSocket, Message message) {
        try {
            SharedObject object = SharedObject.fromByteArray(message.getBytes());
            synchronized (sharedObjects) {
                sharedObjects.put(object.getId(), object);
            }
            broadcast(message, sourceSocket);        
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public void onSharedObjectDelete(Socket sourceSocket, Message message) {
        String objId = new String(message.getBytes());
        synchronized (sharedObjects) {
            sharedObjects.remove(objId);
        }
        broadcast(message);        
    }
    
    @Override
    public void onSharedObjectMove(Socket sourceSocket, Message message) {
        try {
            String objId = new String(message.getBytes());
            Point pos = (Point)NetUtil.fromByteArray(message.getAux());
            synchronized (sharedObjects) {
                SharedObject object = sharedObjects.get(objId);
                if (null != object) {
                    ((WidgetObject)object).setPosition(pos);
                }
            }
            broadcast(message, sourceSocket);        
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public void onClientDisconnect(Socket sourceSocket, Message message) {
        Set<String> heldLocks = new HashSet<>();
        synchronized (clients) {
            System.out.printf("%s disconnected\n", sourceSocket.getRemoteSocketAddress().toString());
            for (Map.Entry<String, Socket> entry : locks.entrySet()) {
                if (entry.getValue().equals(sourceSocket)) {
                    heldLocks.add(entry.getKey());
                }
            }
            for (String heldLock : heldLocks) {
                locks.remove(heldLock);
            }
            clients.remove(sourceSocket);
        }        
    }
    
    private void broadcast(Message message) {
        synchronized (clients) {
            for (Socket client : clients) {
                try {
                    NetUtil.sendMessage(client, message);
                } catch(Exception ex) {
                    onClientDisconnect(client, null);
                }
            }
        }
    }
    
    private void broadcast(Message message, Socket exclude) {
        synchronized (clients) {
            for (Socket client : clients) {
                if (!exclude.equals(client)) {
                    try {
                        NetUtil.sendMessage(client, message);
                    } catch(Exception ex) {
                        onClientDisconnect(client, null);
                    }
                }
            }
        }
    }    
}
