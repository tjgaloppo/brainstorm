package com.galoppo.brainstorm;

import com.galoppo.brainstorm.protocol.Message;
import java.net.Socket;
import com.galoppo.brainstorm.protocol.client.ClientMessageListener;
import com.galoppo.brainstorm.protocol.client.ClientMessageProcessor;
import com.galoppo.brainstorm.protocol.ProtoListener;
import com.galoppo.brainstorm.net.NetUtil;
import com.galoppo.brainstorm.protocol.client.LockProxy;
import com.galoppo.brainstorm.protocol.SharedObject;
import com.galoppo.brainstorm.widget.BaseWidget;
import com.galoppo.brainstorm.widget.WidgetObject;
import com.galoppo.brainstorm.widget.WidgetPanel;
import com.galoppo.brainstorm.widget.factory.WidgetRegistry;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.TooManyListenersException;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

public class Client extends JFrame 
    implements LockProxy, ClientMessageListener, DropTargetListener, ActionListener {
    
    private final Socket serverSocket;
    
    private final JTextArea textArea;
    private final WidgetPanel wPanel;
    private final WidgetRegistry registry;
    
    private final String userName;
    
    public Client(Socket serverSocket) {
        super("Brainstorm!");

        if (null != System.getProperty("brainstorm.username")) {
            userName = System.getProperty("brainstorm.username");
        } else {
            userName = "user";
        }
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        JMenuItem miConnect = new JMenuItem("Connect");
        miConnect.addActionListener(this);
        JMenuItem miDisconnect = new JMenuItem("Disconnect");
        miDisconnect.addActionListener(this);
        JMenu mSession = new JMenu("Session");
        mSession.add(miConnect);
        mSession.add(miDisconnect);
        
        JMenuItem miOpen = new JMenuItem("Open");
        miOpen.addActionListener(this);
        JMenuItem miSave = new JMenuItem("Save");
        miSave.addActionListener(this);
        JMenu mWorkspace = new JMenu("Workspace");
        mWorkspace.add(miOpen);
        mWorkspace.add(miSave);
        
        JMenuItem miAbout = new JMenuItem("About");
        miAbout.addActionListener(this);
        JMenu mHelp = new JMenu("Help");
        mHelp.add(miAbout);
        
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(mSession);
        menuBar.add(mWorkspace);
        menuBar.add(mHelp);
        setJMenuBar(menuBar);
        
        JSplitPane split1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        JSplitPane split2 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        
        registry = new WidgetRegistry();
        
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Root");
        for (String widgetType : registry.getRegisteredTypes()) {
            root.add(new DefaultMutableTreeNode(widgetType));
        }
        JTree tree = new JTree(root);
        JScrollPane treeScroll = new JScrollPane(tree);
        tree.setDragEnabled(true);
        tree.setRootVisible(false);
        split1.add(treeScroll);
                
        wPanel = new WidgetPanel(this);
        split2.add(new JScrollPane(wPanel));

        DropTarget graphDropTarget = new DropTarget();
        try {
            graphDropTarget.addDropTargetListener(this);
        } catch (TooManyListenersException ex) {
            ex.printStackTrace();
        }
        wPanel.setDropTarget(graphDropTarget);        
        wPanel.addActionListener(this);
        
        textArea = new JTextArea();
        textArea.setEditable(false);
        JTextField text2 = new JTextField();
        JSplitPane textPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        textPane.add(new JScrollPane(textArea));
        textPane.add(text2);
        split2.add(textPane);
        
        split1.add(split2);
        add(split1);
        
        setPreferredSize(new Dimension(800,600));
        pack();
        split1.setDividerLocation(1. / 6.);
        split1.setResizeWeight(1./6.);
        split2.setDividerLocation(3. / 4.);
        split2.setResizeWeight(3./4.);
        pack();
        textPane.setDividerLocation(8. / 10.);
        textPane.setResizeWeight(1.0);
        
        text2.addKeyListener(new KeyAdapter(){
            @Override
            public void keyReleased(KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    String txt = text2.getText();
                    try {
                        String txtMsg = String.format("[%s]: %s", userName, txt);
                        NetUtil.sendMessage(serverSocket, Message.TYPE_TEXT, txtMsg.getBytes());
                    } catch(Exception ex) {
                        ex.printStackTrace();
                    }
                    text2.setText("");
                    evt.consume();
                }                
            }
        });
        
        this.serverSocket = serverSocket;
        new Thread(new ProtoListener(serverSocket, new ClientMessageProcessor(this))).start();
        try {
            NetUtil.sendMessage(serverSocket, Message.TYPE_SHARED_REFRESH, new byte[0]);
        } catch(Exception ex){}
    }
    
    @Override
    public void repaint() {
        if (EventQueue.isDispatchThread()) {
            super.repaint();
        } else {
            EventQueue.invokeLater(() -> super.repaint());
        }
    }
    
    @Override
    public boolean acquireLock(String objId) {
        boolean locked = false;
        
        try {
            WidgetObject object = wPanel.get(objId);
            NetUtil.sendMessage(serverSocket, Message.TYPE_LOCK_REQUEST, objId.getBytes());
            synchronized (object) {
                object.wait();
            }
            locked = object.isLocked();
        } catch(Exception ex) {
            
        }
        
        return locked;
    }
    
    @Override
    public void releaseLock(String objId) {
        try {
            NetUtil.sendMessage(serverSocket, Message.TYPE_LOCK_RELEASE, objId.getBytes());
        } catch(Exception ex) {
            
        }
    }
    
    @Override
    public void onTextMessage(Message message) {
        String str = new String(message.getBytes());
        textArea.append(str + "\n");
        textArea.setCaretPosition(textArea.getDocument().getLength());        
    }
    
    @Override
    public void onSharedObjectCreateMessage(Message message) {
        try {
            SharedObject obj = SharedObject.fromByteArray(message.getBytes());
            synchronized (wPanel) {
                wPanel.add((WidgetObject)obj);
            }
            repaint();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public void onSharedObjectUpdateMessage(Message message) {
        try {
            SharedObject obj = SharedObject.fromByteArray(message.getBytes());
            synchronized (wPanel) {
                wPanel.remove(obj.getId());
                wPanel.add((WidgetObject)obj);
            }
            repaint();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public void onSharedObjectMoveMessage(Message message) {
        try {
            String id = new String(message.getBytes());
            Point pos = (Point)NetUtil.fromByteArray(message.getAux());
            synchronized (wPanel) {
                SharedObject obj = wPanel.get(id);
                if (null != obj) {
                    ((WidgetObject)obj).setPosition(pos);
                    wPanel.setSize(wPanel.getPreferredSize());
                }
            }
            repaint();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public void onSharedObjectDeleteMessage(Message message) { 
        String id = new String(message.getBytes());
        synchronized (wPanel) {
            wPanel.remove(id);
        }       
        repaint();
    }
    
    @Override
    public void onLockGrantMessage(Message message) {
        String id = new String(message.getBytes());
        SharedObject obj = wPanel.get(id);
        if(null != obj) {
            synchronized (obj) {
                obj.setLocked(true);
                obj.notify();
            }
        }        
    }
    
    @Override
    public void onLockReleaseMessage(Message message) {
        String id = new String(message.getBytes());
        SharedObject obj = wPanel.get(id);
        if(null != obj) {
            obj.setLocked(false);
        }        
    }
    
    @Override
    public void onLockDenyMessage(Message message) {
        String id = new String(message.getBytes());
        SharedObject obj = wPanel.get(id);
        if(null != obj) {
            synchronized (obj) {
                obj.setLocked(false);
                obj.notify();
            }
        }        
    }
    
    @Override
    public void onDisconnectMessage(Message message) {
        System.out.println("Disconnected.");
    }
    
    @Override
    public void dragEnter(DropTargetDragEvent e){
        
    }
    
    @Override
    public void dragExit(DropTargetEvent e){
        
    }
    
    @Override
    public void dragOver(DropTargetDragEvent e){
        
    }
    
    @Override
    public void drop(DropTargetDropEvent e){
        try {
            String newType = e.getTransferable().getTransferData(DataFlavor.stringFlavor).toString();
            BaseWidget obj = null;
            
            obj = registry.create(newType, e.getLocation().x, e.getLocation().y);
            
            if (null != obj && obj.edit(this)) {
                NetUtil.sendMessage(serverSocket, Message.TYPE_SHARED_NEW, obj);
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    @Override
    public void dropActionChanged(DropTargetDragEvent e){

    } 
    
    private void openWorkspace() {
        JFileChooser dialog = new JFileChooser();
        if (JFileChooser.APPROVE_OPTION == dialog.showOpenDialog(this)) {
            ObjectInputStream ois = null;
            try {
                FileInputStream fis = new FileInputStream(dialog.getSelectedFile());
                ois = new ObjectInputStream(fis);
                Object obj;
                while (null != (obj = ois.readObject())) {
                    if (obj instanceof WidgetObject) {
                        NetUtil.sendMessage(serverSocket, Message.TYPE_SHARED_NEW, (WidgetObject)obj);
                    }
                }
            } catch(EOFException eof) {
                
            } catch(Exception ex) {
                ex.printStackTrace();
            } finally {
                if (null != ois) {
                    try { ois.close(); } catch(Exception ex) {}
                }
            }
        }
    }
    
    private void saveWorkspace() {
        JFileChooser dialog = new JFileChooser();
        if (JFileChooser.APPROVE_OPTION == dialog.showSaveDialog(this)) {
            try {
                FileOutputStream fos = new FileOutputStream(dialog.getSelectedFile());
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                List<WidgetObject> widgets = wPanel.getWidgetList();
                synchronized (widgets) {
                    for (WidgetObject obj : widgets) {
                        oos.writeObject(obj);
                    }
                }
                oos.close();
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            switch (e.getActionCommand().toLowerCase()) {
                case "connect":
                    break;
                    
                case "disconnect":
                    break;
                
                case "open":
                    openWorkspace();
                    break;
                    
                case "save":
                    saveWorkspace();
                    break;
                
                case "edit":
                    NetUtil.sendMessage(serverSocket, Message.TYPE_SHARED_UPD, (SharedObject)e.getSource());
                    break;
                
                case "move":
                    WidgetObject obj = (WidgetObject)e.getSource();
                    NetUtil.sendMessage(serverSocket, Message.TYPE_SHARED_MOVE, 
                            obj.getId().getBytes(),
                            NetUtil.toByteArray(new Point(obj.getX(), obj.getY())));
                    break;
                      
                case "copy":
                    NetUtil.sendMessage(serverSocket, Message.TYPE_SHARED_NEW, ((WidgetObject)e.getSource()).copy());
                    releaseLock(((SharedObject)e.getSource()).getId());
                    break;
                    
                case "delete":
                    NetUtil.sendMessage(serverSocket, Message.TYPE_SHARED_DEL, ((SharedObject)e.getSource()).getId().getBytes());
                    break;
                    
                case "noop":
                    releaseLock(((SharedObject)e.getSource()).getId());
                    break;
            }
        } catch(Exception ex) {
            
        }
    }
}
