package com.galoppo.brainstorm.util;

import java.io.ByteArrayOutputStream;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class Compressor  {
    public static byte[] compress(byte[] inputBytes) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Deflater d = new Deflater(Deflater.BEST_COMPRESSION);
        d.setInput(inputBytes);
        d.finish();
        byte[] buf = new byte[1024];
        while (!d.finished()) {
            int n = d.deflate(buf);
            bos.write(buf, 0, n);
        }
        try { bos.close(); } catch(Exception ex){}
        return bos.toByteArray();
    }
    
    public static byte[] decompress(byte[] inputBytes) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Inflater d = new Inflater();
        d.setInput(inputBytes);
        byte[] buf = new byte[1024];
        try {
            while (!d.finished()) {
                int n = d.inflate(buf);
                bos.write(buf, 0, n);
            }
        } catch(Exception ex) {}
        try { bos.close(); } catch(Exception ex){}
        return bos.toByteArray();
    }
}
