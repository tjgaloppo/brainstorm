package com.galoppo.brainstorm.protocol.client;

import com.galoppo.brainstorm.protocol.Message;

public interface ClientMessageListener {
    public void onTextMessage(Message message);
    public void onSharedObjectCreateMessage(Message message);
    public void onSharedObjectUpdateMessage(Message message);
    public void onSharedObjectMoveMessage(Message message);
    public void onSharedObjectDeleteMessage(Message message);
    public void onLockGrantMessage(Message message);
    public void onLockReleaseMessage(Message message);
    public void onLockDenyMessage(Message message);
    public void onDisconnectMessage(Message message);
}
