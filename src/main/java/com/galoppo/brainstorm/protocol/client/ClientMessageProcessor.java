package com.galoppo.brainstorm.protocol.client;

import com.galoppo.brainstorm.protocol.Message;
import com.galoppo.brainstorm.protocol.MessageProcessor;
import java.net.Socket;

public class ClientMessageProcessor implements MessageProcessor {
    private final ClientMessageListener messageListener;
    
    public ClientMessageProcessor(ClientMessageListener messageListener) {
        this.messageListener = messageListener;
    }
    
    @Override
    public void processMessage(Socket sourceSocket, Message message) {
        switch (message.getType()) {
            case Message.TYPE_TEXT:
                messageListener.onTextMessage(message);
                break;

            case Message.TYPE_SHARED_LOCK:
                messageListener.onLockGrantMessage(message);
                break;

            case Message.TYPE_LOCK_DENY:
                messageListener.onLockDenyMessage(message);
                break;                    

            case Message.TYPE_SHARED_UNLOCK:
                messageListener.onLockReleaseMessage(message);
                break;

            case Message.TYPE_SHARED_NEW:
                messageListener.onSharedObjectCreateMessage(message);
                break;

            case Message.TYPE_SHARED_UPD:
                messageListener.onSharedObjectUpdateMessage(message);
                break;

            case Message.TYPE_SHARED_MOVE:
                messageListener.onSharedObjectMoveMessage(message);
                break;                    

            case Message.TYPE_SHARED_DEL:
                messageListener.onSharedObjectDeleteMessage(message);
                break;

            case Message.TYPE_DISC:
                messageListener.onDisconnectMessage(message);
                break;   
        }
    }
}

