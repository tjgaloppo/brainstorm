package com.galoppo.brainstorm.protocol.client;

public interface LockProxy {
    public boolean acquireLock(String id);
    public void    releaseLock(String id);
}
