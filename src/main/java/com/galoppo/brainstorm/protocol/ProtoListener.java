package com.galoppo.brainstorm.protocol;

import java.net.Socket;

public class ProtoListener implements Runnable {
    private final Socket socket;
    private final MessageProcessor processor;
    
    public ProtoListener(Socket socket, MessageProcessor processor) {
        this.socket = socket;
        this.processor = processor;
    }
    
    @Override
    public void run() {
        try {
            while (true) {
                Message msg = Message.readFrom(socket.getInputStream());
                processor.processMessage(socket, msg);
            }
        } catch(Exception ex) {
            processor.processMessage(socket, new Message(Message.TYPE_DISC, null));
        }
    }
}
