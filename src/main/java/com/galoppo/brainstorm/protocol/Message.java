package com.galoppo.brainstorm.protocol;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;

public class Message implements Serializable {
    private final int msgType;
    private final byte[] msgBytes;
    private byte[] auxBytes;
    
    public static final int TYPE_DISC = -1;
    public static final int TYPE_TEXT = 1;
    
    public static final int TYPE_SHARED_NEW = 0x8001;
    public static final int TYPE_SHARED_DEL = 0x8002;
    public static final int TYPE_SHARED_UPD = 0x8003;
    public static final int TYPE_SHARED_LOCK = 0x8004;
    public static final int TYPE_SHARED_UNLOCK = 0x8005;
    public static final int TYPE_SHARED_MOVE = 0x8006;
    public static final int TYPE_SHARED_REFRESH = 0x8008;
    
    public static final int TYPE_LOCK_REQUEST = 0xC001;
    public static final int TYPE_LOCK_RELEASE = 0xC002;
    public static final int TYPE_LOCK_DENY    = 0xC003;
    
    public Message(int type, byte[] bytes) {
        msgType = type;
        msgBytes = bytes;
        auxBytes = null;
    }
    
    public Message(int type, byte[] bytes, byte[] auxBytes) {
        this(type, bytes);
        this.auxBytes = auxBytes;
    }    
    
    public static Message readFrom(InputStream is) throws Exception {
        ObjectInputStream ois = new ObjectInputStream(is);
        Message msg = (Message) ois.readObject();
        return msg;
    }
    
    public int getType() {
        return msgType;
    }
  
    public byte[] getBytes() {
        return msgBytes;
    }
    
    public byte[] getAux() {
        return auxBytes;
    }
}
