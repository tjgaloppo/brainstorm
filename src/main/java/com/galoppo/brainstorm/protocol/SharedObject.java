package com.galoppo.brainstorm.protocol;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.UUID;

public class SharedObject implements Serializable {
    protected final UUID uuid; 
    private transient boolean locked;
    
    public SharedObject() {
        uuid = UUID.randomUUID();
    }
    
    public String getId() {
        return uuid.toString();
    }
    
    public void setLocked(boolean locked) {
        this.locked = locked;
    }
    
    public boolean isLocked() {
        return locked;
    }
       
    public static SharedObject fromByteArray(byte[] bytes) throws Exception {
        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bytes));
        return (SharedObject) ois.readObject();
    }
}
