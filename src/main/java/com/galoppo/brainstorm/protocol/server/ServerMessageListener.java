package com.galoppo.brainstorm.protocol.server;

import com.galoppo.brainstorm.protocol.Message;
import java.net.Socket;

public interface ServerMessageListener {
    public void onTextMessage(Socket source, Message message);
    public void onLockRequest(Socket source, Message message);
    public void onLockRelease(Socket source, Message message);
    public void onSharedObjectCreate(Socket source, Message message);
    public void onSharedObjectDelete(Socket source, Message message);
    public void onSharedObjectUpdate(Socket source, Message message);
    public void onSharedObjectMove(Socket source, Message message);
    public void onRefreshRequest(Socket source, Message message);
    public void onClientDisconnect(Socket source, Message message);
}
