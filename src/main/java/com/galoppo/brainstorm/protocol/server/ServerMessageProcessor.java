package com.galoppo.brainstorm.protocol.server;

import com.galoppo.brainstorm.protocol.Message;
import com.galoppo.brainstorm.protocol.MessageProcessor;
import java.net.Socket;

public class ServerMessageProcessor implements MessageProcessor {
    private final ServerMessageListener messageListener;
    
    public ServerMessageProcessor(ServerMessageListener messageListener) {
        this.messageListener = messageListener;
    }
    
    @Override
    public void processMessage(Socket sourceSocket, Message message) {
        switch (message.getType()) {
            case Message.TYPE_TEXT:
                messageListener.onTextMessage(sourceSocket, message);
                break;

            case Message.TYPE_LOCK_REQUEST:
                messageListener.onLockRequest(sourceSocket, message);
                break;

            case Message.TYPE_LOCK_RELEASE:
                messageListener.onLockRelease(sourceSocket, message);
                break;

            case Message.TYPE_SHARED_REFRESH:
                messageListener.onRefreshRequest(sourceSocket, message);
                break;

            case Message.TYPE_SHARED_NEW:
                messageListener.onSharedObjectCreate(sourceSocket, message);
                break;                

            case Message.TYPE_SHARED_UPD:
                messageListener.onSharedObjectUpdate(sourceSocket, message);
                break;

            case Message.TYPE_SHARED_MOVE:
                messageListener.onSharedObjectMove(sourceSocket, message);
                break;

            case Message.TYPE_SHARED_DEL:
                messageListener.onSharedObjectDelete(sourceSocket, message);
                break;

            case Message.TYPE_DISC:
                messageListener.onClientDisconnect(sourceSocket, message);
                break;            
        }
    }
}
