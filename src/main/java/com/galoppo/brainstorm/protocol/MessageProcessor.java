package com.galoppo.brainstorm.protocol;

import java.net.Socket;

public interface MessageProcessor {
    public void processMessage(Socket source, Message message);
}
