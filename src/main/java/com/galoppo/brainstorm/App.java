package com.galoppo.brainstorm;

import java.awt.EventQueue;
import java.net.ServerSocket;
import java.net.Socket;

public class App {
    boolean running = true;
    
    public static void main(String[] args) {
        int port = Integer.parseInt(System.getProperty("brainstorm.port", "9999"));
        
        if (args.length == 0) {
            new App().runServer(port);
        } else {
            try {
                Socket sock = new Socket(args[0], port);
                EventQueue.invokeLater(() -> {
                    new Client(sock).setVisible(true);
                });
            } catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }
    
    protected void runServer(int port) {
        final Server server = new Server();
        
        Thread accThread = new Thread(() -> {
            try {
                ServerSocket sockSrv = new ServerSocket(port);
                while (running) {
                    Socket sockClient = sockSrv.accept();
                    server.addClient(sockClient);
                }
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }); 
        accThread.start();
    }
}
