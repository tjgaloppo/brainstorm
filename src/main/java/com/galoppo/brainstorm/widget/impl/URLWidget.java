package com.galoppo.brainstorm.widget.impl;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Frame;
import java.awt.Graphics;
import java.net.URI;

public class URLWidget extends LabelWidget {
    public URLWidget(int x, int y, String url) {
        super(x, y, url);
    }
    
    protected URLWidget(URLWidget obj) {
        super(obj);
    }
    
    public boolean edit(Frame parent) {
        boolean modified = false;
        
        if (null == getText()) {
            setText("http://");
            modified = super.edit(parent);
        } else {
            if (Desktop.isDesktopSupported()) {
                try {
                    Desktop.getDesktop().browse(new URI(getText()));
                } catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        
        return modified;
    }
    
    @Override
    public void draw(Graphics g) {
        super.draw(g);
        g.setFont(getFont());
        Color oldColor = g.getColor();
        g.setColor(Color.BLUE);
        g.drawString(getText(), x+(w-textWidth)/2, y+(h+textHeight-textDescent)/2);    
        g.setColor(oldColor);
    }
    
    @Override
    public URLWidget copy() {
        return new URLWidget(this);
    }
}
