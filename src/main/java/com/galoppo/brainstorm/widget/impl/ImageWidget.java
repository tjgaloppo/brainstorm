package com.galoppo.brainstorm.widget.impl;

import com.galoppo.brainstorm.util.FileIO;
import com.galoppo.brainstorm.widget.AbstractDataWidget;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.io.ByteArrayInputStream;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

public class ImageWidget extends AbstractDataWidget {
    private transient Image image = null;
    
    public ImageWidget(int x, int y) {
        super(x, y, 0, 0);
    }
    
    protected ImageWidget(ImageWidget obj) {
        super(obj);
    }
    
    @Override
    public boolean edit(Frame parent) {
        boolean modified = false; 
        
        JFileChooser dia = new JFileChooser();
        if (dia.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
            try {
                setData(FileIO.loadFile(dia.getSelectedFile()));
                loadImage();
                modified = true;
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }        
        return modified;
    }
    
    public void draw(Graphics g) {
        if (null == image) {
            loadImage();
        }
        if (null != image) {
            g.drawImage(image, x, y, w, h, null);
        } else {
            super.draw(g);
        }
    }
    
    private void loadImage() {
        try {
            image = ImageIO.read(new ByteArrayInputStream(getData()));
            w = image.getWidth(null);
            h = image.getHeight(null);
        } catch(Exception ex) {
            image = null;
        }
    }
    
    @Override
    public ImageWidget copy() {
        return new ImageWidget(this);
    }
}
