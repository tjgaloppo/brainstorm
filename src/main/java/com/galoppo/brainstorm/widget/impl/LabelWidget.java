package com.galoppo.brainstorm.widget.impl;

import com.galoppo.brainstorm.widget.dialog.TextFieldDialog;
import com.galoppo.brainstorm.widget.AbstractTextWidget;
import java.awt.Canvas;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;

public class LabelWidget extends AbstractTextWidget {
    LabelWidget(LabelWidget obj) {
        super(obj);  
        adjustSize();
    }
    
    public LabelWidget(int x, int y, String text){
        super(x, y, text);
        adjustSize();
    }    
    
    @Override
    public LabelWidget copy() {
        return new LabelWidget(this);
    }
    
    @Override
    public boolean edit(Frame parent) {
        boolean modified = false;
        TextFieldDialog dia = new TextFieldDialog(parent, getText());
        dia.setVisible(true);
        if (dia.isAccepted()) {
            setText(dia.getText());
            modified = true;
        }
        return modified;
    }
    
    @Override
    public String toString(){
        return getText();
    }
    
    @Override
    public void draw(Graphics g){
        super.draw(g);
        g.setFont(getFont());
        g.drawString(getText(), x+(w-textWidth)/2, y+(h+textHeight-textDescent)/2);        
    }
    
    @Override
    protected final void adjustSize() {
        Canvas c = new Canvas();
        FontMetrics fm = c.getFontMetrics(getFont());
        if (null != getText()) {
            textWidth = fm.stringWidth(getText());
        } else {
            textWidth = 0;
        }
        
        textHeight = fm.getHeight();
        textDescent = fm.getDescent();
        w = 10 + textWidth;
        h = 10 + textHeight;
    }
}
