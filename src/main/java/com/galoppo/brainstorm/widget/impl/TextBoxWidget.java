package com.galoppo.brainstorm.widget.impl;

import com.galoppo.brainstorm.widget.dialog.TextAreaDialog;
import com.galoppo.brainstorm.widget.AbstractTextWidget;
import java.awt.Canvas;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;

public class TextBoxWidget extends AbstractTextWidget {        
    TextBoxWidget(TextBoxWidget obj) {
        super(obj);
        adjustSize();        
    }
    
    public TextBoxWidget(int x, int y, String text){
        super(x, y, text); 
        adjustSize();
    }    

    @Override
    public String toString(){
        return getText();
    }
    
    @Override
    public TextBoxWidget copy() {
        return new TextBoxWidget(this);
    }
    
    @Override
    public boolean edit(Frame parent) {
        boolean modified = false;
        TextAreaDialog dia = new TextAreaDialog(parent, getText());
        dia.setVisible(true);
        if (dia.isAccepted()) {
            setText(dia.getText());
            modified = true;
        }
        return modified;
    }
    
    @Override
    public void draw(Graphics g){
        super.draw(g);
        g.setFont(getFont());
        String[] parts = getText().split("\n");
        for (int j=0; j<parts.length; j++) {
            g.drawString(parts[j], x+(w-textWidth)/2, y+5+(j+1)*textHeight-textDescent);        
        }
    }
    
    @Override
    protected final void adjustSize() {
        String[] parts = getText().split("\n");
        Canvas c = new Canvas();
        FontMetrics fm = c.getFontMetrics(getFont());
        
        textWidth = fm.stringWidth(parts[0].trim());
        for (int j=1; j<parts.length; j++) {
            int tw = fm.stringWidth(parts[j].trim());
            if (tw > textWidth) textWidth = tw;
        }
        
        textHeight = fm.getHeight();
        textDescent = fm.getDescent();
        
        w = 10 + textWidth;
        h = 10 + textHeight * parts.length;
    }
}
