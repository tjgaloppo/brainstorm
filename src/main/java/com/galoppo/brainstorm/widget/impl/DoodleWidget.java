package com.galoppo.brainstorm.widget.impl;

import com.galoppo.brainstorm.widget.dialog.DoodleDialog;
import com.galoppo.brainstorm.util.Compressor;
import com.galoppo.brainstorm.widget.AbstractDataWidget;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.Arrays;

public class DoodleWidget extends AbstractDataWidget {
    private transient Image image = null;
    
    DoodleWidget(DoodleWidget object) {
        super(object);
        setData(Arrays.copyOf(object.getData(), object.size()));
    }
    
    public DoodleWidget(int x, int y, int w, int h) {
        super(x, y, w+4, h+4);
        byte[] data = new byte[4 * w * h];
        Arrays.fill(data, (byte)255);
        setData(Compressor.compress(data));
    }
    
    @Override
    public boolean edit(Frame parent) {
        boolean modified = false;
        DoodleDialog dia = new DoodleDialog(parent, byteArrayToIntArray(Compressor.decompress(getData())), w-4, h-4);
        dia.setVisible(true);
        if (dia.isAccepted()) {
            int[] data = dia.getData();
            setData(Compressor.compress(intArrayToByteArray(data)));
            image = null;
            modified = true;
        }
        return modified;
    }
    
    @Override
    public void draw(Graphics g) {
        super.draw(g);
        if (null == image) {
            BufferedImage bi = new BufferedImage(w-4, h-4, BufferedImage.TYPE_INT_ARGB);
            bi.setRGB(0, 0, w-4, h-4, byteArrayToIntArray(Compressor.decompress(getData())), 0, w-4);
            image = bi;
        }
        g.drawImage(image, x+2, y+2, null);
    }
    
    @Override 
    public DoodleWidget copy() {
        return new DoodleWidget(this);
    }
    
    private static byte[] intArrayToByteArray(int[] data) {
        ByteBuffer bbuf = ByteBuffer.allocate(data.length * 4);
        IntBuffer  ibuf = bbuf.asIntBuffer();
        ibuf.put(data);
        return bbuf.array();
    }
    
    private static int[] byteArrayToIntArray(byte[] data) {
        IntBuffer ibuf = IntBuffer.allocate(data.length / 4);
        ByteBuffer bbuf = ByteBuffer.wrap(data);
        ibuf.put(bbuf.asIntBuffer());
        return ibuf.array();
    }    
}
