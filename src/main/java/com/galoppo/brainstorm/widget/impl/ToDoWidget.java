package com.galoppo.brainstorm.widget.impl;

import com.galoppo.brainstorm.widget.BaseWidget;
import com.galoppo.brainstorm.widget.dialog.ToDoDialog;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.UIManager;

public class ToDoWidget extends BaseWidget {
    private final Map<String, Boolean> dataMap;
    
    private transient int blockHeight;
    private transient int textDescent;
    
    private static Font font = null;
    static {
        try {
            font = Font.decode("Lucida Sans").deriveFont(20f);
        } catch(Exception ex) {
            font = UIManager.getDefaults().getFont("TabbedPane.font");
        }
    }    
    
    public ToDoWidget(int x, int y) {
        super(x, y, 0, 0);
        dataMap = new LinkedHashMap<>();
    }
    
    protected ToDoWidget(ToDoWidget obj) {
        super(obj);
        dataMap = new LinkedHashMap<>();
        for (Map.Entry<String, Boolean> entry : obj.dataMap.entrySet()) {
            dataMap.put(entry.getKey(), entry.getValue());
        }
    }
       
    @Override
    public boolean edit(Frame parent) {
        boolean modified = false;
        ToDoDialog dia = new ToDoDialog(parent, dataMap);
        dia.setVisible(true);
        if (dia.isAccepted()) {
            resize();
            modified = true;
        }
        return modified;
    }
    
    private void resize() {
        Canvas c = new Canvas();
        FontMetrics fm = c.getFontMetrics(font);
        blockHeight = 2 + fm.getHeight();
        textDescent = fm.getDescent();
        h = 4 + dataMap.size() * blockHeight;
        int maxw = 0;
        for (String str : dataMap.keySet()) {
            maxw = Math.max(maxw, fm.stringWidth(str));
        }
        w = maxw + blockHeight + 8;
    }
    
    @Override
    public ToDoWidget copy() {
        return new ToDoWidget(this);
    }
    
    @Override
    public void draw(Graphics g) {
        if (blockHeight == 0) {
            resize();
        }
        super.draw(g);
        g.setFont(font);
        Color originalColor = g.getColor();
        int index = 0;
        int left = x + 2, right = x + w - 2;
        for (Map.Entry<String, Boolean> item : dataMap.entrySet()) {
            int top = y + 2 + index * blockHeight;
            int bottom = top + blockHeight - 1;
            g.setColor(Color.WHITE);
            g.drawLine(left, top, right, top);
            g.drawLine(left, top, left, bottom);    
            g.setColor(Color.BLACK);
            g.drawLine(left, bottom, right, bottom);
            g.drawLine(right, top, right, bottom);
            g.drawString(item.getKey(), left + 2 + blockHeight + 1, top + blockHeight - textDescent - 1);
            g.setColor(item.getValue() ? Color.GREEN : Color.RED);
            g.fillRect(left+1, top+1, blockHeight - 3, blockHeight - 3);
            index += 1;
        }
        g.setColor(originalColor);
    }
}
