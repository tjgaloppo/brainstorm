package com.galoppo.brainstorm.widget.impl;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;

public class EquationWidget extends TextBoxWidget implements Cloneable {
    private transient Image image = null;
    
    EquationWidget(EquationWidget obj) {
        super(obj);
    }
    
    public EquationWidget(int x, int y, String text) {
        super(x, y, text);
    }
    
    @Override
    public EquationWidget copy() {
        return new EquationWidget(this);
    }
    
    @Override
    public void setText(String text) {
        super.setText(text);
        image = null;
    }
    
    @Override
    public void draw(Graphics g){
        if (null == image) {
            try {
                TeXFormula formula = new TeXFormula(getText());
                image = formula.createBufferedImage(TeXConstants.STYLE_DISPLAY, 20.0f, Color.BLACK, Color.WHITE);
            } catch(Exception ex) {
                image = null;
            }
        }
        if (null != image) {
            w = image.getWidth(null);
            h = image.getHeight(null);
            g.drawImage(image, x, y, null);
        } else {
            String errorText = "Equation Error";
            FontMetrics fm = g.getFontMetrics();
            w = 10 + fm.stringWidth(errorText);
            h = 10 + fm.getHeight();
            String temp = getText();
            setText(errorText);
            super.draw(g);   
            setText(temp);
        }
    }    
}
