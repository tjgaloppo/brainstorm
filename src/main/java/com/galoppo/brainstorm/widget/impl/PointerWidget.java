package com.galoppo.brainstorm.widget.impl;

import com.galoppo.brainstorm.widget.BaseWidget;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import javax.imageio.ImageIO;

public class PointerWidget extends BaseWidget {
    private static Image image;
    static {
        try {
            image = ImageIO.read(PointerWidget.class.getClassLoader().getResourceAsStream("pointer.png"));
        } catch(Exception ex) {}
    }
    
    public PointerWidget(int x, int y) {
        super(x, y, image.getWidth(null), image.getHeight(null));
    }
    
    protected PointerWidget(PointerWidget obj) {
        super(obj);
    }
    
    @Override
    public void draw(Graphics g) {
        g.setXORMode(Color.WHITE);
        g.drawImage(image, x, y, null);
        g.setPaintMode();
    }
    
    @Override
    public boolean edit(Frame parent) {
        return true;
    }
    
    @Override
    public PointerWidget copy() {
        return new PointerWidget(this);
    }
}
