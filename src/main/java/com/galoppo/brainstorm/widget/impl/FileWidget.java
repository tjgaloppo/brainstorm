package com.galoppo.brainstorm.widget.impl;

import com.galoppo.brainstorm.util.FileIO;
import com.galoppo.brainstorm.widget.AbstractDataWidget;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

public class FileWidget extends AbstractDataWidget {
    private String fileName;
    
    private static Image icon;
    static {
        try {
            icon = ImageIO.read(FileWidget.class.getClassLoader().getResourceAsStream("file_icon.png"));
        } catch(Exception ex) {}
    }
    
    public FileWidget(int x, int y) {
        super(x, y, icon.getWidth(null), icon.getHeight(null));
    }
    
    protected FileWidget(FileWidget obj) {
        super(obj);
    }
    
    @Override
    public boolean edit(Frame parent) {
        boolean modified = false; 
        
        JFileChooser dia = new JFileChooser();
        if (null == getData()) {
            if (dia.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
                try {
                    setData(FileIO.loadFile(dia.getSelectedFile()));
                    fileName = dia.getSelectedFile().getName();
                    modified = true;
                } catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
        } else {
            dia.setSelectedFile(new File(fileName));
            if (dia.showSaveDialog(parent) == JFileChooser.APPROVE_OPTION) {
                try {
                    FileIO.saveFile(dia.getSelectedFile(), getData());
                } catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        
        return modified;
    }
    
    @Override
    public void draw(Graphics g) {
        FontMetrics fm = g.getFontMetrics();
        int strW = fm.stringWidth(fileName);
        int strH = fm.getHeight();
        int txtX = x + (w - strW) / 2;
        int txtY = y + h + strH;        
        g.drawImage(icon, x, y, null);
        g.drawString(fileName, txtX, txtY);
    }    
    
    @Override
    public FileWidget copy() {
        return new FileWidget(this);
    }
}
