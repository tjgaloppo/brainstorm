package com.galoppo.brainstorm.widget.dialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class DoodleDialog 
  extends AbstractWidgetDialog
  implements ActionListener, MouseListener, MouseMotionListener {
    private final BufferedImage image;
    private final JButton bOk;
    private final JButton bClear;
    private final JButton bCancel;
    private final JPanel  imagePanel;
    private final JCheckBox cbKalman;
    
    private int mx, my;
    private KalmanMouse kfilt = null;
    
    private static class KalmanMouse {
        private final RealMatrix F = MatrixUtils.createRealMatrix(new double[][]{ {1, 1}, {0, 1} });
        private final RealMatrix Q = MatrixUtils.createRealMatrix(new double[][]{ {0.25, 0.5} , {0.5, 1.0} });
        private final RealVector H = MatrixUtils.createRealVector(new double[]{ 1, 0 });
        private final double     R = 40 * 40;
        
        private RealMatrix Px = MatrixUtils.createRealMatrix(new double[][]{ {0, 0}, {0, 0} });
        private RealMatrix Py = MatrixUtils.createRealMatrix(new double[][]{ {0, 0}, {0, 0} });
        private RealVector x;
        private RealVector y;
        
        public KalmanMouse(int mx, int my) {
            x = MatrixUtils.createRealVector(new double[]{ mx, 0 });
            y = MatrixUtils.createRealVector(new double[]{ my, 0 });
        }
        
        public Point update(int mx, int my) {
            Object[] xupd = kalman(Px, x, mx);
            Object[] yupd = kalman(Py, y, my);
            x = (RealVector)xupd[1];
            y = (RealVector)yupd[1];
            Px = (RealMatrix)xupd[2];
            Py = (RealMatrix)yupd[2];
            return new Point(((Double)xupd[0]).intValue(), ((Double)yupd[0]).intValue());
        }
        
        private Object[] kalman(RealMatrix P, RealVector x, double z) {
            RealVector xp = F.operate(x);
            RealMatrix Pp = F.multiply(P).multiply(F.transpose()).add(Q);
            double y  = z - H.dotProduct(xp);
            double S  = Pp.preMultiply(H).dotProduct(H) + R;
            RealVector K  = Pp.operate(H).mapDivide(S);
            RealVector xu = xp.add(K.mapMultiply(y));
            RealMatrix Pu = MatrixUtils.createRealIdentityMatrix(2).subtract(K.outerProduct(H)).multiply(Pp);
            return new Object[]{ H.dotProduct(xu), xu, Pu };
        }
    }
    
    public DoodleDialog(Frame parent, int[] rasterData, int w, int h) {
        super(parent);
        
        setTitle("Doodle Editor");
        
        image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        image.setRGB(0, 0, w, h, rasterData, 0, w);
        image.getGraphics().setColor(Color.BLACK);
        
        imagePanel = new JPanel(){
            @Override
            public void paint(Graphics g) {
                super.paint(g);
                //g.drawImage(image, (getWidth()-image.getWidth()) / 2, 
                //        (getHeight() - image.getHeight()) / 2, null);
                g.drawImage(image, 0, 0, null);
            }      
        };
        imagePanel.addMouseListener(this);
        imagePanel.addMouseMotionListener(this);
        imagePanel.setPreferredSize(new Dimension(w,h));
        
        JButton bBlack = new JButton(" ");
        bBlack.setBackground(Color.BLACK);
        bBlack.addActionListener(this);
        JButton bRed = new JButton(" ");
        bRed.setBackground(Color.RED);
        bRed.addActionListener(this);
        JButton bOrange = new JButton(" ");
        bOrange.setBackground(Color.ORANGE);
        bOrange.addActionListener(this);
        JButton bYellow = new JButton(" ");
        bYellow.setBackground(Color.YELLOW);
        bYellow.addActionListener(this);
        JButton bGreen = new JButton(" ");
        bGreen.setBackground(Color.GREEN);
        bGreen.addActionListener(this);
        JButton bBlue = new JButton(" ");
        bBlue.setBackground(Color.BLUE);
        bBlue.addActionListener(this);
        JButton bPurple = new JButton(" ");
        bPurple.setBackground(Color.MAGENTA);
        bPurple.addActionListener(this);
        JButton bWhite = new JButton(" ");
        bWhite.setBackground(Color.WHITE);
        bWhite.addActionListener(this);
        JPanel colorPanel = new JPanel();
        colorPanel.add(bBlack);
        colorPanel.add(bRed);
        colorPanel.add(bOrange);
        colorPanel.add(bYellow);
        colorPanel.add(bGreen);
        colorPanel.add(bBlue);
        colorPanel.add(bPurple);
        colorPanel.add(bWhite);
        
        JPanel optionPanel = new JPanel();
        cbKalman = new JCheckBox("Kalman motion");
        bClear = new JButton("Clear");
        bClear.addActionListener(this);
        cbKalman.setSelected(true);
        optionPanel.add(cbKalman);
        optionPanel.add(bClear);
        
        JSplitPane split3 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        split3.add(colorPanel);
        split3.add(optionPanel);
        
        bOk = new JButton("OK");
        bCancel = new JButton("Cancel");
        bOk.addActionListener(this);
        bCancel.addActionListener(this);        
        JSplitPane split2 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        split2.add(bOk);
        split2.add(bCancel);
        
        JSplitPane split1 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        split1.add(split3);
        split1.add(split2);
        
        JSplitPane split0 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        split0.add(imagePanel);
        split0.add(split1);
        
        add(split0);
        setSize(w, h+175);
        pack();
        split0.setResizeWeight(1.0);
        split2.setDividerLocation(0.5);
        split2.setResizeWeight(0.5);
        
        setLocation();
    }
    
    public int[] getData() {
        return image.getRGB(0,0,image.getWidth(),image.getHeight(),null,0,image.getWidth());
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
        
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
        
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
        mx = e.getX();
        my = e.getY();
        if (cbKalman.isSelected()) {
            kfilt = new KalmanMouse(mx, my);
        } else {
            kfilt = null;
        }
        repaint();
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
        kfilt = null;
        repaint();
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        
    }
    
    @Override
    public void mouseMoved(MouseEvent e) {
        
    }
    
    @Override
    public void mouseDragged(MouseEvent e) {
        Point pnt;
        if (null != kfilt) {
            pnt = kfilt.update(e.getX(), e.getY());
        } else {
            pnt = new Point(e.getX(), e.getY());
        }
        Graphics g = image.getGraphics();
        g.setColor(imagePanel.getForeground());
        g.drawLine(mx, my, (int)pnt.getX(), (int)pnt.getY());
        mx = (int)pnt.getX();
        my = (int)pnt.getY();
        repaint();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(bOk)) {
            setAccepted(true);
            setVisible(false);
        } else if (e.getSource().equals(bCancel)) {
            setAccepted(false);
            setVisible(false);
        } else if (e.getSource().equals(bClear)) {
            int[] whiteArray = new int[image.getWidth() * image.getHeight()];
            for(int i=0; i<whiteArray.length;i++){
                whiteArray[i] = -1;
            }
            image.setRGB(0, 0, image.getWidth(), image.getHeight(), whiteArray, 0, image.getWidth());
            repaint();
        } else if (e.getSource() instanceof JButton) {
            imagePanel.setForeground(((JButton)e.getSource()).getBackground());
        }
    }
}
