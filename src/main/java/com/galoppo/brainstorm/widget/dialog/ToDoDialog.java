package com.galoppo.brainstorm.widget.dialog;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ToDoDialog 
  extends AbstractWidgetDialog 
  implements ActionListener
{
    private final Map<String, Boolean> items;
    private final DefaultTableModel tableModel;
    private final JTable table;
    
    public ToDoDialog(Frame parent, Map<String, Boolean> items) {
        super(parent);
        
        this.items = items;
        
        setTitle("To-Do Editor");
        
        tableModel = new DefaultTableModel(items.size(), 2){
            @Override
            public Class getColumnClass(int n) {
                if (n == 0) return String.class;
                else if (n == 1) return Boolean.class;
                else return Object.class;
            }
        };
        tableModel.setColumnIdentifiers(new String[]{"Action", "Complete"});
        int row = 0;
        for (Map.Entry<String, Boolean> entry : items.entrySet()) {
            tableModel.setValueAt(entry.getKey(), row, 0);
            tableModel.setValueAt(entry.getValue(), row, 1);
            row += 1;
        }        
        
        table = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
        
        JSplitPane pane1 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        pane1.add(scrollPane);
        
        JButton bOk = new JButton("Ok");
        bOk.addActionListener(this);
        JButton bCancel = new JButton("Cancel");
        bCancel.addActionListener(this);
        JSplitPane pane2 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        pane2.add(bOk);
        pane2.add(bCancel);
        pane1.add(pane2);
        add(pane1);
        
        setSize(300, 175);
        pack();
        pane1.setDividerLocation(0.9);
        pane1.setResizeWeight(1.0);
        pane2.setDividerLocation(0.5);
        pane2.setResizeWeight(0.5);
        table.getColumnModel().getColumn(0).setPreferredWidth(280);
        table.getColumnModel().getColumn(0).setResizable(false);
        table.getColumnModel().getColumn(1).setPreferredWidth(20);
        table.getColumnModel().getColumn(1).setResizable(false);
        table.addMouseListener(new MouseAdapter(){ 
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
                    tableModel.addRow(new Object[]{ "", false });
                }
            }
        });
        table.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                    if (table.getSelectedRow() != -1 && !table.isEditing()) {
                        tableModel.removeRow(table.getSelectedRow());
                        e.consume();
                    }
                }
            }
        });
        
        setLocation();
    }
    
    private void setItems() {
        items.clear();
        for (int j=0; j<tableModel.getRowCount(); j++) {
            items.put((String)tableModel.getValueAt(j, 0), (Boolean)tableModel.getValueAt(j, 1));
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand().toLowerCase()) {
            case "ok":
                setItems();
                setAccepted(true);
                setVisible(false);
                break;
                
            case "cancel":
                setAccepted(false);
                setVisible(false);
                break;
        }
    }
}
