package com.galoppo.brainstorm.widget.dialog;

import java.awt.Frame;
import javax.swing.JDialog;

public abstract class AbstractWidgetDialog extends JDialog {
    private boolean accepted = false;
    private final Frame parent;
    
    public AbstractWidgetDialog(Frame parent) {
        super(parent);
        this.parent = parent;
        setModalityType(JDialog.ModalityType.APPLICATION_MODAL);
    }
    
    protected void setLocation() {
        if (null != parent) {
            setLocation(parent.getX() + (parent.getWidth()-getWidth()) / 2, 
                    parent.getY() + (parent.getHeight() - getHeight()) / 2);
        }  
    }
    
    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }
    
    public boolean isAccepted() {
        return accepted;
    }
}
