package com.galoppo.brainstorm.widget.dialog;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

public class TextFieldDialog extends AbstractWidgetDialog implements ActionListener {
    private final JTextField textField;
    private final JButton buttonOk;
    private final JButton buttonCancel;
    
    public TextFieldDialog(Frame parent, String text) {
        super(parent);
        
        setTitle("Edit Text Field");
        
        textField = new JTextField(text);
        buttonOk  = new JButton("OK");
        buttonCancel = new JButton("Cancel");
         
        buttonOk.addActionListener(this);
        buttonCancel.addActionListener(this);
        
        JSplitPane pane0 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        pane0.setEnabled(false);
        pane0.add(textField);
        JSplitPane pane1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        pane1.setEnabled(false);
        pane1.add(buttonOk);
        pane1.add(buttonCancel);
        pane0.add(pane1);
        add(pane0);
        
        setPreferredSize(new Dimension(350,200));
        pack();
        pane0.setDividerLocation(0.5);
        pane0.setResizeWeight(0.5);
        pane1.setDividerLocation(0.5);
        pane1.setResizeWeight(0.5);
        
        setLocation();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        setAccepted(e.getActionCommand().equals(buttonOk.getText()));
        setVisible(false);
    }
    
    public String getText() {
        return textField.getText();
    }
}
