package com.galoppo.brainstorm.widget;

import java.awt.Font;
import javax.swing.UIManager;

public abstract class AbstractTextWidget extends BaseWidget {
    private String text;
    
    protected int textWidth;
    protected int textHeight;
    protected int textDescent;
    
    private static Font font = null;
    static {
        try {
            font = Font.decode("Lucida Sans").deriveFont(14f);
        } catch(Exception ex) {
            font = UIManager.getDefaults().getFont("TabbedPane.font");
        }
    }
    
    protected AbstractTextWidget(AbstractTextWidget obj) {
        super(obj);
        this.text = obj.text;
        this.textWidth = obj.textWidth;
        this.textHeight = obj.textHeight;
        this.textDescent = obj.textDescent;
    }
    
    public AbstractTextWidget(int x, int y, String text) {
        super(x, y, 0, 0);
        this.text = text;
    }
    
    protected abstract void adjustSize();
    
    public String getText(){
        return text;
    }
    
    public void setText(String text){
        this.text = text;       
        adjustSize();
    }           
    
    protected Font getFont() {
        return font;
    }
}
