package com.galoppo.brainstorm.widget;

import java.util.Arrays;

public abstract class AbstractDataWidget extends BaseWidget {
    private byte[] data;
    
    protected AbstractDataWidget(AbstractDataWidget obj) {
        super(obj);
        data = Arrays.copyOf(obj.data, obj.data.length);
    }
    
    public AbstractDataWidget(int x, int y, int w, int h) {
        super(x, y, w, h);
        data = null;
    }
    
    public byte[] getData() {
        return data;
    }
    
    public void setData(byte[] data) {
        this.data = data;
    }
    
    public int size() {
        return null != data ? data.length : 0;
    }
}
