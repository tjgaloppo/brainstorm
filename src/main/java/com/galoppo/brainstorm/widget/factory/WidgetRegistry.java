package com.galoppo.brainstorm.widget.factory;

import com.galoppo.brainstorm.widget.BaseWidget;
import com.galoppo.brainstorm.widget.impl.DoodleWidget;
import com.galoppo.brainstorm.widget.impl.EquationWidget;
import com.galoppo.brainstorm.widget.impl.FileWidget;
import com.galoppo.brainstorm.widget.impl.ImageWidget;
import com.galoppo.brainstorm.widget.impl.LabelWidget;
import com.galoppo.brainstorm.widget.impl.PointerWidget;
import com.galoppo.brainstorm.widget.impl.TextBoxWidget;
import com.galoppo.brainstorm.widget.impl.ToDoWidget;
import com.galoppo.brainstorm.widget.impl.URLWidget;
import java.util.HashMap;
import java.util.Map;

public class WidgetRegistry {
    private final Map<String, Factory> registry;
    
    public WidgetRegistry() {
        registry = new HashMap<>();
        registry.put("Doodle", (x, y) -> new DoodleWidget(x, y, 400, 300));
        registry.put("Equation", (x, y) -> new EquationWidget(x, y, "\\frac{1}{2}"));
        registry.put("File", (x, y) -> new FileWidget(x, y));
        registry.put("Image", (x, y) -> new ImageWidget(x, y));
        registry.put("Label", (x, y) -> new LabelWidget(x, y, "Text Label"));
        registry.put("TextBox", (x, y) -> new TextBoxWidget(x, y, "Text Area"));
        registry.put("Pointer", (x, y) -> new PointerWidget(x, y));
        registry.put("URL", (x, y) -> new URLWidget(x, y, null));
        registry.put("To Do", (x, y) -> new ToDoWidget(x, y));
    }
    
    public BaseWidget create(String type, int x, int y) {
        BaseWidget object = null;
        if (registry.containsKey(type)) {
            object = registry.get(type).create(x,y);
        }
        return object;
    }
    
    public String[] getRegisteredTypes() {
        return registry.keySet().toArray(new String[0]);
    }
}
