package com.galoppo.brainstorm.widget.factory;

import com.galoppo.brainstorm.widget.BaseWidget;

public interface Factory {
    public BaseWidget create(int x, int y);
}
