package com.galoppo.brainstorm.widget;

import com.galoppo.brainstorm.protocol.SharedObject;
import java.awt.Graphics;
import java.awt.Point;

public abstract class WidgetObject extends SharedObject {
    protected int x, y;
    protected int w, h;
    
    public abstract WidgetObject copy();
    
    WidgetObject(WidgetObject wObj) {
        this.x = wObj.x+5;
        this.y = wObj.y+5;
        this.w = wObj.w;
        this.h = wObj.h;
    }
    
    WidgetObject(int x, int y, int w, int h){
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }
    
    public void setPosition(Point p) {
        this.x = p.x;
        this.y = p.y;
    }
    
    public int getX(){
        return x;
    }
    
    public int getY(){
        return y;
    }
    
    public int getWidth(){
        return w;
    }
    
    public int getHeight(){
        return h;
    }
    
    public abstract void draw(Graphics g);
    
    public void move(int dx, int dy){
        x += dx;
        y += dy;
    }
    
    public boolean contains(Point p){
        return p.getX() >= x && p.getX() < x+w && 
                p.getY() >= y && p.getY() < y+h;
    }
}
