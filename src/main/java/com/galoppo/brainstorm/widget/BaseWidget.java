package com.galoppo.brainstorm.widget;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;

public abstract class BaseWidget extends WidgetObject {
    protected Color color;
    
    protected BaseWidget(BaseWidget wObj) {
        super(wObj);
        this.color = wObj.color;
    }
    
    public BaseWidget(int x, int y, int w, int h){
        super(x, y, w, h);
        this.color = Color.LIGHT_GRAY;
    }
    
    public abstract boolean edit(Frame parent);
    
    @Override
    public void draw(Graphics g) {  
        Color oldColor = g.getColor();
        g.setColor(color);
        g.fillRect(x, y, w, h);
        
        g.setColor(Color.BLACK);
        g.drawLine(x+w, y, x+w, y+h);
        g.drawLine(x+w, y+h, x, y+h);
        g.drawLine(x+1, y+1, x+w-1, y+1);
        g.drawLine(x+1, y+1, x+1, y+h-1);
        
        g.setColor(Color.WHITE);
        g.drawLine(x, y, x+w-1, y);
        g.drawLine(x, y, x, y+h-1);
        g.drawLine(x+w-1, y+1, x+w-1, y+h-1);
        g.drawLine(x+w-1, y+h-1, x+1, y+h-1);
        
        g.setColor(oldColor);
    }
}
