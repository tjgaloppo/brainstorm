package com.galoppo.brainstorm.widget;

import com.galoppo.brainstorm.dialog.WidgetPopup;
import com.galoppo.brainstorm.protocol.client.LockProxy;
import java.awt.Dimension;
import java.awt.Frame;
import java.util.List;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class WidgetPanel extends JPanel implements MouseMotionListener, MouseListener {
    protected static int MODE_MOVE = 0;
    
    protected LockProxy lockProxy;
    protected final List<WidgetObject> objects;
    protected final Map<String, WidgetObject> objectMap;
    protected Point mouseDownAt, currentPoint;
    protected WidgetObject selectedObject = null;
    protected int mouseMode;
    
    protected final List<ActionListener> actionListeners = new LinkedList<>();
    
    public WidgetPanel(LockProxy lockProxy){
        super();
        setDoubleBuffered(true);
        addMouseListener(this);
        addMouseMotionListener(this);
        this.lockProxy = lockProxy;
        objects = new ArrayList<>();
        objectMap = new HashMap<>();
    }
    
    public void clear(){
        synchronized(objects) { 
            objects.clear();
            objectMap.clear();
        }
    }
    
    public List<WidgetObject> getWidgetList(){
        return objects;
    }
    
    public void addActionListener(ActionListener listener) {
        actionListeners.add(listener);
    }
    
    @Override
    public void paint(Graphics g){
        Image img = this.createImage(getWidth(), getHeight());
        Graphics b = img.getGraphics();
        super.paint(b);
        
        synchronized (objects) {
            for(WidgetObject object : objects){
                object.draw(b);
            }
        }
        
        g.drawImage(img, 0, 0, this);
    }
    
    public void add(WidgetObject object){
        synchronized(objects) { 
            objects.add(object);
            objectMap.put(object.getId(), object);
        }
        repaint();
        setSize(getPreferredSize());
    }
    
    public WidgetObject get(String objId) {
        WidgetObject obj = null;
        synchronized (objects) {
            obj = objectMap.get(objId);
        }
        return obj;
    }
    
    public void remove(String wid) {
        synchronized(objects) { 
            objects.removeIf((x) -> x.getId().equals(wid)); 
            objectMap.remove(wid);
        }
        repaint();
        setSize(getPreferredSize());
    }
    
    public void remove(WidgetObject object) {
        remove(object.getId());
    }
    
    @Override
    public void mouseDragged(MouseEvent e){
        if(null != selectedObject){
            if(mouseMode == MODE_MOVE){
                selectedObject.move(e.getX() - mouseDownAt.x, e.getY() - mouseDownAt.y);
                notifyAction(selectedObject, 0, "move");
                mouseDownAt = e.getPoint();
            } 
            repaint();
            setSize(getPreferredSize());
        }
        e.consume();
    }
    
    private void notifyAction(WidgetObject object, int id, String command) {
        ActionEvent event = new ActionEvent(object, id, command);
        for (ActionListener listener : actionListeners) {
            listener.actionPerformed(event);
        }
    }
    
    @Override
    public Dimension getPreferredSize() {
        int maxX = 0;
        int maxY = 0;
        for (WidgetObject object : objects) {
            int tx = object.getX() + object.getWidth();
            int ty = object.getY() + object.getHeight();
            if (tx > maxX) {
                maxX = tx;
            }
            if (ty > maxY) {
                maxY = ty;
            }
        }
        return new Dimension(maxX+1, maxY+1);
    }
    
    @Override
    public void mouseMoved(MouseEvent e){
        e.consume();
    }
    
    @Override
    public void mouseClicked(MouseEvent e){
        e.consume();
    }
    
    @Override
    public void mouseEntered(MouseEvent e){
        e.consume();
    }
    
    @Override
    public void mouseExited(MouseEvent e){
        e.consume();
    }
    
    @Override
    public void mousePressed(MouseEvent e){
        if(e.isPopupTrigger()){
            onPopupMenuTrigger(e);
        } else if(e.getButton() == MouseEvent.BUTTON1){
            if (selectedObject != null) {
                lockProxy.releaseLock(selectedObject.getId());
                selectedObject = null;
            }
   
            mouseDownAt = e.getPoint();
            
            for (int j=0; j<objects.size(); j++) {
                WidgetObject obj = objects.get(objects.size() - j - 1);
                if(obj.contains(mouseDownAt)){
                    if (lockProxy.acquireLock(obj.getId())) {
                        selectedObject = obj;
                    }
                    break;
                }
            }
            
            if(null != selectedObject){
                if(e.getClickCount() == 1){
                    mouseMode = MODE_MOVE;
                } else if(e.getClickCount() == 2){
                    if(selectedObject instanceof BaseWidget){
                        BaseWidget widget = (BaseWidget)selectedObject;
                        boolean modified = false;
                        if (lockProxy.acquireLock(widget.getId())) {
                            modified = ((BaseWidget)selectedObject).edit((Frame)this.getTopLevelAncestor());
                            lockProxy.releaseLock(widget.getId());
                        }
                        if (modified) {
                            notifyAction(selectedObject, 0, "edit");
                        }
                        repaint();
                    }
                }
            }
        }
        e.consume();
    }
    
    @Override
    public void mouseReleased(MouseEvent e){
        if (e.isPopupTrigger()) {
            onPopupMenuTrigger(e);
        } else {
            if(null != selectedObject){
                lockProxy.releaseLock(selectedObject.getId());
                selectedObject = null;
                repaint();
            }
        }
        e.consume();
    }   
    
    private void onPopupMenuTrigger(MouseEvent e) {
        WidgetObject target = null;
            
        for (int j=0; j<objects.size(); j++) {
            WidgetObject obj = objects.get(objects.size() - j - 1);
            if (obj.contains(e.getPoint())) {
                if (lockProxy.acquireLock(obj.getId())) {
                    target = obj;
                }
                break;
            }
        }

        if (null != target) {
            WidgetPopup popMenu = new WidgetPopup(target);
            for (ActionListener listener : actionListeners) {
                popMenu.addActionListener(listener);
            }
            popMenu.show(this, e.getX(), e.getY());       
        }
    }
}
