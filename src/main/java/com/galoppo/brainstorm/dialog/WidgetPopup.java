package com.galoppo.brainstorm.dialog;

import com.galoppo.brainstorm.widget.WidgetObject;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

public class WidgetPopup
  extends JPopupMenu 
  implements ActionListener, PopupMenuListener  {
    private final WidgetObject targetObject;
    private final List<ActionListener> actionListeners = new LinkedList<>();
    
    public WidgetPopup(WidgetObject targetObject) {
        super();
        
        JMenuItem itCopy = new JMenuItem("Copy");
        JMenuItem itRemove = new JMenuItem("Delete");
        
        itCopy.addActionListener(this);
        itRemove.addActionListener(this);
        
        add(itCopy);
        add(itRemove);
        addPopupMenuListener(this);
    
        this.targetObject = targetObject;
    }
    
    public void addActionListener(ActionListener actionListener) {
        actionListeners.add(actionListener);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        ActionEvent event = new ActionEvent(targetObject, 0, e.getActionCommand().toLowerCase());
        for (ActionListener actionListener : actionListeners) {
            actionListener.actionPerformed(event);
        }
    }
    
    @Override
    public void popupMenuCanceled(PopupMenuEvent e) {
        ActionEvent event = new ActionEvent(targetObject, 0, "noop");
        for (ActionListener actionListener : actionListeners) {
            actionListener.actionPerformed(event);
        }
    }
    
    @Override
    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        
    }
        
    @Override
    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        
    }
}
