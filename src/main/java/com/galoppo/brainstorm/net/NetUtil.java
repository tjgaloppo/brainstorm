package com.galoppo.brainstorm.net;

import com.galoppo.brainstorm.protocol.Message;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;

public class NetUtil {   
    public static void sendMessage(Socket dest, int type, Serializable object) throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(object);
        byte[] bytes = bos.toByteArray();
        oos.close();
        sendMessage(dest, type, bytes);
    }
    
    public static void sendMessage(Socket dest, int type, byte[] bytes) throws Exception {
        sendMessage(dest, new Message(type, bytes));
    }   

    public static void sendMessage(Socket dest, int type, byte[] bytes, byte[] auxBytes) throws Exception {
        sendMessage(dest, new Message(type, bytes, auxBytes));
    }   
    
    
    public static void sendMessage(Socket dest, Message message) throws Exception {
        ObjectOutputStream oos = new ObjectOutputStream(dest.getOutputStream());
        oos.writeObject(message);
    }
    
    public static byte[] toByteArray(Serializable object) throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(object);
        byte[] bytes = bos.toByteArray();
        oos.close(); 
        return bytes;
    } 
    
    public static Object fromByteArray(byte[] bytes) throws Exception {
        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bytes));
        return ois.readObject();
    }
}
